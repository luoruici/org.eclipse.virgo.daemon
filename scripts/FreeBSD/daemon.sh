#!/bin/bash

VIRGO_PATH="/usr/local/virgo"
JAVA_PATH="/usr/local/bin/java"
JSVC_PATH="/usr/local/bin/jsvc"

cd $VIRGO_PATH

classpath=""
frameworkclasspath=""

for f in $VIRGO_PATH/lib/*.jar
do
	classpath=$classpath:$f
	frameworkclasspath=$frameworkclasspath,file:$f
done

for f in $VIRGO_PATH/plugins/org.eclipse.osgi_*.jar
do
	classpath=$classpath:$f
	frameworkclasspath=$frameworkclasspath,file:$f
done

for f in $VIRGO_PATH/plugins/org.eclipse.equinox.console.ssh_*.jar
do
	classpath=$classpath:$f
done

jvmoptions="\
	-Xmx512m\
	-XX:MaxPermSize=512m\
	-XX:+HeapDumpOnOutOfMemoryError\
	-XX:ErrorFile=$VIRGO_PATH/serviceability/error.log\
	-XX:HeapDumpPath=$VIRGO_PATH/serviceability/heap_dump.hprof"


jvmproperties="\
	-Dcom.sun.management.jmxremote.port=9875\
	-Dcom.sun.management.jmxremote.authenticate=true\
	-Dcom.sun.management.jmxremote.login.config=virgo-kernel\
	-Dcom.sun.management.jmxremote.access.file=$VIRGO_PATH/configuration/org.eclipse.virgo.kernel.jmxremote.access.properties\
	-Djavax.net.ssl.keyStore=$VIRGO_PATH/configuration/keystore\
	-Djavax.net.ssl.keyStorePassword=changeit\
	-Dcom.sun.management.jmxremote.ssl=true\
	-Dcom.sun.management.jmxremote.ssl.need.client.auth=false\
	-Djava.security.auth.login.config=$VIRGO_PATH/configuration/org.eclipse.virgo.kernel.authentication.config\
	-Dorg.eclipse.virgo.kernel.authentication.file=$VIRGO_PATH/configuration/org.eclipse.virgo.kernel.users.properties\
	-Djava.io.tmpdir=$VIRGO_PATH/work/tmp\
	-Dorg.eclipse.virgo.kernel.home=$VIRGO_PATH\
	-Dorg.eclipse.virgo.kernel.config=$VIRGO_PATH/configuration\
	-Dosgi.java.profile=file:$VIRGO_PATH/configuration/java6-server.profile\
	-Declipse.ignoreApp=true\
	-Dosgi.install.area=$VIRGO_PATH\
	-Dosgi.configuration.area=$VIRGO_PATH/work\
	-Dssh.server.keystore=$VIRGO_PATH/configuration/hostkey.ser\
	-Dosgi.frameworkClassPath=$frameworkclasspath
	-Djava.endorsed.dirs=$VIRGO_PATH/lib/endorsed"

startclass="org.eclipse.virgo.daemon.VirgoDaemon"
startoptions="-noExit"

STOP_OPT=""
if [ "$1" == "stop" ]; then
	STOP_OPT="-stop"
fi

$JSVC_PATH -debug $STOP_OPT -outfile /tmp/virgo.out -errfile /tmp/virgo.err  $jvmoptions $jvmproperties -cp $classpath $startclass $startoptions


