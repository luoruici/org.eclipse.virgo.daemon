package org.eclipse.virgo.daemon;

import org.apache.commons.daemon.Daemon;
import org.apache.commons.daemon.DaemonContext;
import org.apache.commons.daemon.DaemonInitException;

public class VirgoDaemon implements Daemon {
	
	private String[] startupArgs = null;
	private String[] shutdownArgs = {"-jmxport", "9875"};
	
	@Override
	public void destroy() {

	}

	@Override
	public void init(DaemonContext arg0) throws DaemonInitException, Exception {
		startupArgs = arg0.getArguments();
	}

	@Override
	public void start() throws Exception {
		org.eclipse.equinox.launcher.Main.main(startupArgs);
	}

	@Override
	public void stop() throws Exception {
		org.eclipse.virgo.nano.shutdown.ShutdownClient.main(shutdownArgs);
	}
}
